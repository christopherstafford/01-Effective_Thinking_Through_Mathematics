# Week 1
## The Shaky Story Resolved
### Reflect and Respond

**To attack this difficult Shaky Story puzzle, we raised questions repeatedly. These questions were helpful in that they allowed us to focus our thinking on questions that evoked the key insights that led to the solution of the original question. So please describe why the Shaky Story invited you to raise different questions rather than immediately working on the original Shaky Story. Raising questions is a key habit of effective thinking.**


Working through the problem head on would prove to be too much of a challenge. Though solvable it would take considerable amounts of trial and error. By find the essence of the problem and breaking it down into it's smallest part, it allowed us to grow the solution organically.
