# Week 1: The Meanie Genie Puzzle
## Differnt Stragevies
### Reflect and Respond

**Describe why 1 versus 1 as a first weighing will not work. Describe why 2 versus 2 as a first weighing will not work.** 

**If Scott had systematically gone through all the possible first weighings (1 vs. 1, 2 vs. 2, 3 vs 3, 4 vs. 4), would he have come to the solution more quickly?**

1 and 1 wouldn't work because you only have 2 weigh ins, if each weigh in produced a balanced result you would still be left with 4 stones.

2 and 2 wouldn't work because if one of your first weigh in does not produce a weighted result or if both your weigh ins do not produce a balanced result you would be left with 2 stones to choose from.

I can see coming to the solution quicker if you move through the problem systematically.
