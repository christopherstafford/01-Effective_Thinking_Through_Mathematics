# The Meanie Genie Puzze
## Reflect and Respond
Essentially what is being asked in this riddle is of 9 identical looking stones, can you determine the one that is slightly heavier than the others. You are given two chances to compare the weight of any number of stones.

### Statement of the question
Given two single use balance scales, can you determine the single heavier stone of a group of nine. Why or why not? 
